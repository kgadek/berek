#!/usr/bin/env python3
import argparse
import os
from berek.src.git_utils import Git
from berek.src.parsers import Parsers


class BerekMethodNotImplemented(RuntimeError):
    pass


def build_parser():
    """Builds command-line argument parser."""

    args_parser = argparse.ArgumentParser()
    args_subparsers = args_parser.add_subparsers()

    list_subparser = args_subparsers.add_parser('list-formats', aliases=['list', 'lst', 'l'])
    list_subparser.set_defaults(func=main_list)

    make_subparser = args_subparsers.add_parser('make', aliases=['mk', 'm'])
    make_subparser.set_defaults(func=main_make)
    make_subparser.add_argument('tagname',
                                help="Tag name to create. Suggested name scheme: vMAJOR.MINOR.PATCH "
                                     "(cf. semantic versioning).")
    make_subparser.add_argument('-n', '--dry-run', action='store_true')
    make_subparser.add_argument('-r', '--repository-path')
    make_subparser.add_argument('--since')

    return args_parser


def main_make(args):
    """Called when 'make' command is used."""
    repo_path = os.path.abspath(args.repository_path)
    git = Git(repo_path)

    if args.since:
        commit = args.since
        changelog = git.get_changelog_since_given_commit(commit)
    else:
        tag = git.get_previous_tag()
        print('Last found tag: {}'.format(tag))
        changelog = git.get_changelog_since_given_tag(tag)

    print('\nChangelog:\n{}'.format('-'*80))
    print('{}\n'.format(changelog))

    if args.dry_run:
        print('--dry-run option set, no tagging to be done')
    else:
        print('tagging')
        git.set_tag(args.tagname, changelog)


def main_list(args):
    """Called when 'list-formats' command is used."""
    parsers = Parsers().get_parsers()
    for parser in parsers:
        print(parser.print_format())


def main():
    """Parses command-line arguments and dispatches the execution."""
    parser = build_parser()
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
