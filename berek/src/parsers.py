from textwrap import dedent


class ParserInputValidationException(Exception):
    pass


class Parsers:
    def get_parsers(self):
        return self.__class__.__subclasses__()


class Parser1(Parsers):
    @staticmethod
    def parse(input_string):
        changelog_index = input_string.lower().find('changelog\n')
        if changelog_index == -1:
            raise ParserInputValidationException
        changelog_index_end = changelog_index + len('changelog\n')
        return input_string[changelog_index_end:]

    @staticmethod
    def print_format():
        return dedent("""
                        Parser1 changelog format:

                            <Commit name>

                            <commit description>
                            Changelog
                            <actual changelog>
                        """)


class Parser2(Parsers):
    @staticmethod
    def parse(input_string):
        changelog_index = input_string.lower().find('changelog')
        changelog_index_end = changelog_index + len('changelog')
        if changelog_index_end != len(input_string):
            raise ParserInputValidationException
        return input_string[:changelog_index].split('\n')[0]

    @staticmethod
    def print_format():
        return dedent("""
                        Parser2 changelog format1:
                            <All this will be included in changelog> Changelog

                        Parser2 changelog format2:
                            <Commit name which will be included in changelog>

                            <commit description>
                            Changelog
                        """)


def parse_commit(input_string):
    parsers = Parsers().get_parsers()
    for parser in parsers:
        try:
            return parser.parse(input_string)
        except ParserInputValidationException:
            pass
    else:
        return ''
