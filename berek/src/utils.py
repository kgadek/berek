import os
from contextlib import contextmanager


@contextmanager
def cwd(new_dir):
    old_dir = os.path.abspath(os.curdir)
    try:
        os.chdir(new_dir)
        yield
    finally:
        os.chdir(old_dir)

