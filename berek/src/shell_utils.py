import subprocess


def run_command(command):
    p = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    out, err = p.communicate()

    if p.returncode > 0:
        raise subprocess.CalledProcessError(cmd=command, returncode=p.returncode, output=(out + err))

    return out.strip()
