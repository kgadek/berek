import os

from berek.src.shell_utils import run_command
from berek.src.utils import cwd
from berek.src.parsers import parse_commit
from berek.src.git_repo import GitRepo


class Git(GitRepo):
    def __init__(self, repository_path, git_path='git'):
        super().__init__(git_command=git_path, repo_dir=repository_path)
        self.git_path = git_path
        self.repository_path = repository_path

    def get_previous_tag(self):
        with cwd(self.repository_path):
            return self.describe('--tags', '--abbrev=0').strip()

    def get_commit_messages_list_since_given_tag(self, tag):
        with cwd(self.repository_path):
            commits = self._GitRepo__run_command('rev-list', ['--reverse', 'HEAD', '^{}'.format(tag)])

            commit_messages = [
                self.log('--format=%B', '-n1', line)
                for line in commits.splitlines()
            ]

            return commit_messages

    def get_commit_messages_since_given_tag(self, tag):
        commit_messages = '\n'.join(self.get_commit_messages_list_since_given_tag(tag))
        return commit_messages.strip()

    def get_changelog_since_given_tag(self, tag):
        commit_messages = self.get_commit_messages_list_since_given_tag(tag)

        changelog_list = [
            parse_commit(commit)
            for commit in commit_messages
        ]

        changelogs = '\n'.join(
            [
                changelog
                for changelog in changelog_list
                if changelog != ''
            ]
        )

        return changelogs.strip()

    def get_commit_messages_list_since_given_commit(self, commit_number):
        with cwd(self.repository_path):
            commits = self._GitRepo__run_command('rev-list', ['--reverse', 'HEAD', '^{}'.format(commit_number)])

            commit_messages = [
                self.log('--format=%B', '-n1', line).strip()
                for line in commits.splitlines()
            ]

            return commit_messages

    def get_commit_messages_since_given_commit(self, commit_number):
        commit_messages = '\n'.join(self.get_commit_messages_list_since_given_commit(commit_number))
        return commit_messages.strip()

    def get_changelog_since_given_commit(self, commit_number):
        commit_messages = self.get_commit_messages_list_since_given_commit(commit_number)

        changelog_list = [
            parse_commit(commit)
            for commit in commit_messages
        ]

        changelogs = '\n'.join(
            [
                changelog
                for changelog in changelog_list
                if changelog != ''
            ]
        )

        return changelogs.strip()

    def set_tag(self, tag, msg):
        with cwd(self.repository_path):
            with open('changelog', 'w+') as f:
                f.write(msg)
            self.tag('-a', tag, '-F', 'changelog')
            os.remove('changelog')
