import shutil
import tempfile
import unittest
import os

import subprocess

import errno

from berek.src.git_repo import GitRepo
from berek.src.utils import cwd


class Test(unittest.TestCase):
    def load_string_from_file(self, filename):
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_files', filename)
        with open(path, encoding="utf-8") as f:
            return f.read()

    def get_test_workspace_dir(self):
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), 'workspace')


class TestWithGitRepo(Test):
    def setUp(self):

        assert str(self.test_repo_variant_id), \
            "Could not use 'self.test_repo_variant_id'. Have you set it in your Test's __init__?"

        self.temporary_directory = tempfile.mkdtemp()
        try:

            # This is the worst. It seems that the current directory is already removed by the
            # time this method is running. This causes some unexpected failures, for example
            # the os.getcwd() yields FileNotFoundError.
            # This little fellow makes the problem disappear.
            os.chdir("..")

            repo_commands = self.load_string_from_file(
                'test_git_commands_{}'.format(str(self.test_repo_variant_id))
            )
            repo_commands = repo_commands.splitlines()

            with cwd(self.temporary_directory):
                for command in repo_commands:
                    subprocess.check_output(command,
                                            shell=True,
                                            stderr=subprocess.STDOUT)

            self.git_repo = GitRepo(repo_dir=self.temporary_directory)

        except Exception:
            self.tearDown()
            raise

    def tearDown(self):
        try:
            shutil.rmtree(self.temporary_directory)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise e

