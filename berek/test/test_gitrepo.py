from berek.src.git_repo import GitRepo
from berek.test.base_test import TestWithGitRepo


class TestGitRepo(TestWithGitRepo):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_repo_variant_id = 1

    def test_status(self):
        git_repo = GitRepo(repo_dir=self.temporary_directory)

        log_n1 = str(git_repo.log("-n1")).splitlines()
        self.assertEqual(
            "Author: John Zażółć Gęślą Jaźń 🔥⽕ Doe <john+somethingsomething@localhost>",
            log_n1[1]
        )
        self.assertIn(
            "commit3",
            log_n1[4]
        )
