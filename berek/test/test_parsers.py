#!/usr/bin/env python3
import unittest
from berek.test.base_test import Test

from berek.src.parsers import parse_commit


class TestParsers(Test):
    def test_parse_commit_1(self):
        commit = self.load_string_from_file('test_parsers_commit_1')
        commit_parsed = self.load_string_from_file('test_parsers_commit_1_parsed')
        self.assertEqual(parse_commit(commit), commit_parsed)

    def test_parse_commit_2(self):
        commit = self.load_string_from_file('test_parsers_commit_2')
        commit_parsed = self.load_string_from_file('test_parsers_commit_2_parsed')
        self.assertEqual(parse_commit(commit), commit_parsed)

    def test_parse_commit_3(self):
        commit = self.load_string_from_file('test_parsers_commit_3')
        self.assertEqual(parse_commit(commit), '')

    def test_parse_commit_4(self):
        commit = self.load_string_from_file('test_parsers_commit_4')
        commit_parsed = self.load_string_from_file('test_parsers_commit_4_parsed')
        self.assertEqual(parse_commit(commit), commit_parsed)

    def test_parse_commit_5(self):
        commit = self.load_string_from_file('test_parsers_commit_5')
        commit_parsed = self.load_string_from_file('test_parsers_commit_5_parsed')
        self.assertEqual(parse_commit(commit), commit_parsed)

if __name__ == '__main__':
    unittest.main()
